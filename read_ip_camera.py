# USAGE
# How to run the code
# python read_ip_camera.py
# Type "q" without the quotes to stop the program at runtime

# import libraries
import cv2
import numpy as np
import urllib.request as ur

while True:
	# specify url of the IP camera
	url = "http://192.168.20.10"

	# access url and read frames 
	# from camera

	frame = ur.urlopen(url)
	read_frame = frame.read()

	# convert read_frame into byte
	img = np.array(bytearray(read_frame), dtype=np.uint8)
	img = cv2.imdecode(img, -1)

	# show the image
	cv2.imshow("frame", img)
	if(cv2.waitKey(0) & 0xFF) == ord('q'):
		exit(0)
		
cv2.destroyAllWindows()
